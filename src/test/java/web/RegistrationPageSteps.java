package web;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import web.pages.RegistrationPage;
import java.util.Objects;

public class RegistrationPageSteps {

    private static RegistrationPageSteps registrationPageSteps;

    private final RegistrationPage REGISTRATION_PAGE;

    private RegistrationPageSteps() {
        REGISTRATION_PAGE = RegistrationPage.getInstance();
    }

    public static RegistrationPageSteps getInstance() {
        if (Objects.isNull(registrationPageSteps)) registrationPageSteps = new RegistrationPageSteps();
        return registrationPageSteps;
    }

    @Step("Set \"{value}\" in text field \"{field}\"")
    public void setValueInTextField(String field, String value) {
        System.out.println("\n-- Set \"" + value + "\" in text field \"" + field + "\"");
        switch (field) {
            case  ("First Name"):
                REGISTRATION_PAGE.inputFirstName.setValue(value);
                break;
            case ("Last Name"):
                REGISTRATION_PAGE.inputLastName.setValue(value);
                break;
            case ("Email"):
                REGISTRATION_PAGE.inputEmail.setValue(value);
                break;
            case ("Phone Number"):
                REGISTRATION_PAGE.inputPhoneNumber.setValue(value);
                break;
            default:
                System.out.println("\n Select valid text field");
                break;
        }
    }

    @Step("Set gender \"{value}\"")
    public void setGender(String value) {
        System.out.println("\n-- Set gender\"" + value +  "\"");
        if (value.equals("Male")) {
            REGISTRATION_PAGE.radiobuttonMale.click();
            REGISTRATION_PAGE.radiobuttonMale.shouldBe(Condition.selected.because("Male is not selected"));
        }
        else if (value.equals("Female")) {
            REGISTRATION_PAGE.radiobuttonFemale.click();
            REGISTRATION_PAGE.radiobuttonFemale.shouldBe(Condition.selected.because("Female is not selected"));
        }
        else {
            System.out.println("\n Select Male or Female");
        }
    }

    @Step("Set argeement")
    public void setArgeement() {
        System.out.println("\n-- Set argeement");
        REGISTRATION_PAGE.checkboxAgreement.click();
    }

    @Step("Click submit button")
    public void clickSubmitButton() {
        System.out.println("\n-- Click submit button");
        REGISTRATION_PAGE.submitButton.click();
    }


    @Step("Check error of text field \"{field}\"")
    public void checkErrorOfTextField(String field) {
        System.out.println("\n-- Check error of text field\"" + field +  "\"");
        REGISTRATION_PAGE.ErrorOfTextField(field).shouldBe(Condition.visible);
    }

    @Step("Check error of text field \"{field}\" with value \"{value}\"")
    public void checkErrorOfTextFieldWithValue(String field, String value) {
        System.out.println("\n-- Check error of text field\"" + field + "\" with value \"" + value + "\"");
        setValueInTextField(field, value);
        clickSubmitButton();
        REGISTRATION_PAGE.ErrorOfTextField(field).shouldBe(Condition.visible);
    }

    @Step("Check NO error of text field \"{field}\" with value \"{value}\"")
    public void checkNoErrorOfTextFieldWithValue(String field, String value) {
        System.out.println("\n-- Check error of text field\"" + field + "\" with value \"" + value + "\"");
        setValueInTextField(field, value);
        clickSubmitButton();
        REGISTRATION_PAGE.ErrorOfTextField(field).shouldNotBe(Condition.visible);
    }

    @Step("Check error of field \"Gender\"")
    public void checkErrorRequiredGender() {
        System.out.println("\n-- Check error of field \"Gender\"");
        REGISTRATION_PAGE.errorEmptyGender.shouldBe(Condition.visible);
        setGender("Female");
        REGISTRATION_PAGE.errorEmptyGender.shouldNotBe(Condition.visible);
    }

    @Step("Check error no agreement")
    public void checkErrorNoAgreement() {
        System.out.println("\n-- Check error no agreement");
        REGISTRATION_PAGE.errorAgreement.shouldBe(Condition.visible);
        setArgeement();
        REGISTRATION_PAGE.errorAgreement.shouldNotBe(Condition.visible);
    }
}
