package web;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.*;
import web.data.ErrorEmailData;
import web.data.ErrorNameData;
import web.data.ErrorPhoneData;

public class RegistrationTests {

    String url="https://vladimirwork.github.io/web-ui-playground/";

    private RegistrationPageSteps registrationPageSteps;

    private void init() {
        registrationPageSteps = RegistrationPageSteps.getInstance();
    }

    @BeforeClass (description = "Registration tests")
    public void prepareTests() {
        System.out.println("\nRegistration tests");
        init();
    }

    @BeforeTest
    public void open() {
        Selenide.open(url);
    }

    @Test (description = "Successful registration")
    public void successfulRegistrationTest() {
        System.out.println("\n========== Successful registration==========");
        registrationPageSteps.setValueInTextField("First Name","Ivan");
        registrationPageSteps.setValueInTextField("Last Name","Ivanov");
        registrationPageSteps.setValueInTextField("Email","ivanov@test.com");
        registrationPageSteps.setValueInTextField("Phone Number","89000000000");
        registrationPageSteps.setGender("Male");
        registrationPageSteps.setArgeement();
        registrationPageSteps.clickSubmitButton();
        // добавить проверку на открытие страницы личного кабинета или сообщения об успешной регистрации
    }
        // необходима заглушка на метод регистрации с одними и теми же данными
        // необходима проверка на невозможность регистрации с данными уже зарегистированного пользователя

    @Test (description = "Check required fields")
    public void checkRequiredFieldsTest() {
        System.out.println("\n========== Check required fields ==========");
        registrationPageSteps.clickSubmitButton();
        registrationPageSteps.checkErrorOfTextField("First Name");
        registrationPageSteps.checkErrorOfTextField("Last Name");
        registrationPageSteps.checkErrorOfTextField("Email");
        registrationPageSteps.checkErrorOfTextField("Phone Number");
        registrationPageSteps.checkErrorRequiredGender();
        registrationPageSteps.checkErrorNoAgreement();
    }

    @Test (description = "Check First Name and Last Name fields", dataProvider = "errorNameData", dataProviderClass = ErrorNameData.class)
    public void checkTextFieldsTest(String text) {
        System.out.println("\n========== Check First Name and Last Name fields ==========");
        registrationPageSteps.checkErrorOfTextFieldWithValue("First Name",text);
        registrationPageSteps.checkErrorOfTextFieldWithValue("Last Name",text);
    }

    @Test (description = "Check Email field", dataProvider = "errorEmailData", dataProviderClass = ErrorEmailData.class)
    public void checkEmailFieldTest(String email) {
        System.out.println("\n========== Check Email field ==========");
        registrationPageSteps.checkErrorOfTextFieldWithValue("Email",email);
    }

    @Test (description = "Check phone number field", dataProvider = "errorPhoneData", dataProviderClass = ErrorPhoneData.class)
    public void checkPhoneNumberFieldTest(String phone) {
        System.out.println("\n========== Check phone number field ==========");
        registrationPageSteps.checkErrorOfTextFieldWithValue("Phone Number",phone);
    }

    @AfterTest
    public static void tearDown() {
        Selenide.closeWebDriver();
    }
}
