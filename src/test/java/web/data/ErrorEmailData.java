package web.data;

import io.github.sskorol.core.DataSupplier;

public class ErrorEmailData {

    @DataSupplier
    public String[] errorEmailData() {
        return new String[]{"ivanivan",
                            "ivanov@test",
                            "ivanov.com",
                            "ivanov@test@com",
                            "Иванов@test.com"};

    }
}
