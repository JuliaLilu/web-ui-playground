package web.data;

import io.github.sskorol.core.DataSupplier;

public class ErrorNameData {

    @DataSupplier
    public String[] errorNameData() {
        return new String[]{"I",
                            "IvanIvanIvanIvanIvanIvanIv",
                            "   "};

    }
}
