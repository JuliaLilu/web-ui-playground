package web.data;

import io.github.sskorol.core.DataSupplier;

public class ErrorPhoneData {

    @DataSupplier
    public String[] errorPhoneData() {
        return new String[]{"123456",
                            "1234567891234",
                            "12345678aa23",
                            "12345678#_23",
                            "+123456789"};

    }
}
