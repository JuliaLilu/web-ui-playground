package web.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import com.codeborne.selenide.SelenideElement;
import java.util.Objects;

public class RegistrationPage {

    private static RegistrationPage registrationPage;

    public static RegistrationPage getInstance() {
        if (Objects.isNull(registrationPage)) registrationPage = new RegistrationPage();
        return registrationPage;
    }

    public static SelenideElement inputFirstName =  $("[name=\"FirstName\"]");
    public static SelenideElement inputLastName = $("[name=\"LastName\"]");
    public static SelenideElement inputEmail = $("[name=\"Email\"]");
    public static SelenideElement inputPhoneNumber =  $("[name=\"PhoneNumber\"]");
    public static SelenideElement radiobuttonMale =  $("[value=\"Male\"]");
    public static SelenideElement radiobuttonFemale =  $("[value=\"Female\"]");
    public static SelenideElement checkboxAgreement = $("[name=\"Agreement\"]");

    public static SelenideElement submitButton =  $("[name=\"submitbutton\"]");

    public static SelenideElement errorFirstName = $x("//p[text()='Valid first name is required.']");
    public static SelenideElement errorLastName = $x("//p[text()='Valid last name is required.']");
    public static SelenideElement errorEmail = $x("//p[text()='Valid email is required.']");
    public static SelenideElement errorPhoneNumber = $x("//p[text()='Valid phone number is required.']");

    public static SelenideElement errorEmptyGender =  $x("//p[text()='Choose your gender.']");
    public static SelenideElement errorAgreement = $x("//p[text()='You must agree to the processing of personal data.']");

    public SelenideElement ErrorOfTextField(String field) {
        SelenideElement errorOfTextField = null;
        switch (field) {
            case ("First Name"):
                errorOfTextField = errorFirstName;
                break;
            case ("Last Name"):
                errorOfTextField = errorLastName;
                break;
            case ("Email"):
                 errorOfTextField = errorEmail;
                break;
            case ("Phone Number"):
                 errorOfTextField = errorPhoneNumber;
                break;
            default:
                System.out.println("\n Select valid text field");
                break;
        }
        return errorOfTextField;
    }
}